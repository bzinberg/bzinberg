## Welcome!

Here you will find some of my side projects and interests.

* [Snék](https://gitlab.com/bzinberg-misc/snek) -- a snake game that takes place in the Poincaré disk model of the hyperbolic plane.  My first project in Clojure.  I especially like the way namespaced keywords and `spec` together allow per-field informal semantics and documentation to be shared between different data structures that have fields in common, as e.g. I do for the geometric primitives [here](https://gitlab.com/bzinberg-misc/snek/-/blob/5d0f15fa8423b296ea4c0f23c4311da4b864a5ad/src/sn%C3%A9k/core.clj#L7-135).

* [Bell Timer](https://gitlab.com/ben0/belltimer) -- a hardware timer that strikes a bell using a push/pull solenoid when time is up.  My first electronics project, a hobby I picked up during the pandemic.

* [The Interface of an Interface, the Spec of a Spec](https://drive.google.com/file/d/1QUnOPkJBh7Nzw8ubdRibXrA0jjWJ_9H-/view) -- a short talk about the use of informal specifications and interfaces in software engineering, including where they shine for both software development and team/org cohesion; as well as their limitations and the need to hold a broad view of what forms of informal spec strike the right tradeoff for the needs of a given project.

* [Lecture Notes for Design and Analysis of Algorithms](https://ocw.mit.edu/courses/6-046j-design-and-analysis-of-algorithms-spring-2012/pages/lecture-notes/) -- this was a fun and fairly massive project that I put together under the commission of MIT OpenCourseWare while I was a student.  Starting with scans of the crib notes that the professors had prepared for themselves prior to each lecture, I fleshed out the exposition, filled in the remaining details of the pseudocode and analysis, and added high-quality diagrams drawn in [Asymptote](https://asymptote.sourceforge.io/).

* [PoseComposition.jl](https://probcomp.github.io/PoseComposition.jl/dev/) (source repo [here](https://github.com/probcomp/PoseComposition.jl)) -- Most of the research code I wrote at MIT was internal, but this particular library was ubiquitous enough that it was worth carefully factoring out into a public package.  As explained in the docs, this library provides clear data structures and ergonomic algebraic operations on poses, considered either as an absolute pose in the world coordinate frame, or as a relative pose in the coordinate frame determined by another pose; as well as change-of-coordinates operations on point clouds as were frequently used in our robotics work.

* [AVL Tree implementation](https://gitlab.com/bzinberg/avl_tree) -- self-balancing binary trees were a topic I happened to never encounter in an algorithms course; developing this library filled that gap.  The repo contains an AVL tree implementation with the interface of an ordered multi-set, as well as a companion package for rendering such trees in GraphViz.

* [Hangouts viewer](https://gitlab.com/bzinberg/hangouts_viewer/) -- I wish this didn't have to be written, but the chat history that Google made available via Takeout when they sunsetted "Classic" Google Hangouts is not human-readable.  This small Python package (see readme) converts the humongous JSON file into a static site that reads like a chat history, including images and non-message events like users joining and leaving.

Here are some projects currently in progress:

* I'm half way through the CMU [Deep Learning Systems](https://dlsyscourse.org/) course, which was offered as a MOOC in Fall 2022.  My progress is checked in to [gitlab.com/dlsyscourse-bzinberg](https://gitlab.com/dlsyscourse-bzinberg).  I'm particularly interested in extensions of automatic differentiation that allow for change of charts on the parameter manifold, as well as gradient steps that take Riemannian structure into account.  Taken together, these two should allow one to optimize an orientation-valued parameter (on the Riemannian manifold $`SO_3`$) without encountering the [gimbal lock](https://en.wikipedia.org/wiki/Gimbal_lock) that happens near singularities when one identifies orientations with their Euler angle representation.  A demo of this phenomenon is not really within the scope of the course, but I might make it anyway!

* My next embedded project is a wearable featuring two receiver + electroluminescent wire (with EL driving circuitry) assemblies controlled by a single Bluetooth LE remote.
